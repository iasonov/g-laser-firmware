#include <math.h>
/*
#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38*/
#define X_STEP_PIN         46
#define X_DIR_PIN          48
#define X_ENABLE_PIN       62
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15
/*
#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62*/
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define Z_STEP_PIN         26
#define Z_DIR_PIN          28
#define Z_ENABLE_PIN       24

#define Q_STEP_PIN         36
#define Q_DIR_PIN          34
#define Q_ENABLE_PIN       30

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13

#define FAN_PIN            9

#define PS_ON_PIN          12
#define KILL_PIN           -1

#define HEATER_0_PIN       10
#define HEATER_1_PIN       8
#define TEMP_0_PIN          13   // ANALOG NUMBERING
#define TEMP_1_PIN          14   // ANALOG NUMBERING

#define XSTEPMM (3200/48.5)
#define ZSTEPMM (1000/15)
#define YSTEPMM (3217/50.4)

#define Minute 60.0*1000.0*1000.0
void setup() {

  pinMode(LED_PIN  , OUTPUT);
  
  pinMode(X_STEP_PIN  , OUTPUT);
  pinMode(X_DIR_PIN    , OUTPUT);
  pinMode(X_ENABLE_PIN    , OUTPUT);
 
  
  pinMode(Y_STEP_PIN  , OUTPUT);
  pinMode(Y_DIR_PIN    , OUTPUT);
  pinMode(Y_ENABLE_PIN    , OUTPUT);
  
  pinMode(Z_STEP_PIN  , OUTPUT);
  pinMode(Z_DIR_PIN    , OUTPUT);
  pinMode(Z_ENABLE_PIN    , OUTPUT);

  pinMode(X_MIN_PIN,INPUT);
  digitalWrite(X_MIN_PIN,HIGH);
  pinMode(Y_MIN_PIN,INPUT);
  digitalWrite(Y_MIN_PIN,HIGH);
  Serial.begin(115200);
}
boolean dX=false;
boolean dY=false;
boolean dZ=false;
long int toX=0;
long int toY=0;
long int toZ=0;
long int posX=0;
long int posY=0;
long int posZ=0;

float f=1000;


//circular

double R;
double RStep;
double aStart;
double aCurrent;
double cX;
double cY;
double aEnd;
boolean dir=false;
boolean circ=false;
float fX,fY, I,J;
//circular

boolean idle=true;
boolean renullMode=false;
boolean pause=false;
boolean xstep=false;
boolean ystep=false;
boolean zstep=false;

int workMode=0;

void led(){
  if(!idle){
    if (millis() %1000 <500) 
      digitalWrite(LED_PIN, HIGH);
    else
      digitalWrite(LED_PIN, LOW);
  }else{
      digitalWrite(LED_PIN, HIGH);
  }
}

void sendXTo(long int to){
  long int pos = posX-to;
        toX=abs(pos);
          dX=abs(pos)==pos;
          idle=false;
}

void sendYTo(long int to){
  long int pos = posY-to;
  toY=abs(pos);
          dY=abs(pos)==pos;
          idle=false;
}

void sendZTo(long int to){
  long int pos = posZ-to;
  toZ=abs(pos);
          dZ=abs(pos)==pos;
          idle=false;
}

void initG3(){
 float aX= posX/XSTEPMM;
 float aY= posY/YSTEPMM;
 cX=aX+I;
 cY=aY+J;
 fX-=cX;
 fY-=cY;
 
 cX*=XSTEPMM;
 cY*=YSTEPMM;
 aStart = atan2(-J,-I);
 aEnd = atan2(fY,fX);
 Serial.println(String(fX,DEC)+" "+String(fY,DEC));
 Serial.println(aStart/M_PI*180);
 Serial.println(aEnd/M_PI*180);
 R=sqrt(fX*fX+fY*fY);
 if(aEnd<=aStart)
    aEnd+=2*M_PI;
  RStep=2/(R*XSTEPMM);

 aCurrent=aStart;
 circ=true;
 dir=false;
}

void initG2(){
 float aX= posX/XSTEPMM;
 float aY= posY/YSTEPMM;
 cX=aX+I;
 cY=aY+J;
 fX-=cX;
 fY-=cY;
 
 cX*=XSTEPMM;
 cY*=YSTEPMM;
 aStart = atan2(-J,-I);
 aEnd = atan2(fY,fX);
 Serial.println(String(fX,DEC)+" "+String(fY,DEC));
 Serial.println(aStart/M_PI*180);
 Serial.println(aEnd/M_PI*180);
 R=sqrt(fX*fX+fY*fY);
 if(aEnd>=aStart)
    aEnd-=2*M_PI;
  RStep=2/(R*XSTEPMM);

 aCurrent=aStart;
 circ=true;
 dir=true;
}

void commandProcCircular(){
  if(toX==0 && toY==0 && toZ==0){
    aCurrent+=RStep*(dir?-1:1);
    if((dir && aCurrent<aEnd) || (!dir && aCurrent>aEnd )){
      circ=false;
      workMode=1;
      sendXTo(fX*XSTEPMM+cX);
      sendYTo(fY*YSTEPMM+cY);
      return;
    }
    sendXTo(cos(aCurrent)*R*XSTEPMM+cX);
    sendYTo(sin(aCurrent)*R*YSTEPMM+cY);
  }
}



void processCommand(String buf){
  Serial.println(buf);
  String posSub=buf.substring(1);
      float pos=posSub.toFloat();
  switch(buf[0]){
    case 'G':{
      workMode=pos;
      break;
    }
        case 'M':
          idle=false;
          break;
        case 'D':
          if(posSub=="0"){
            toY=0;
            toX=0;
            toZ=0;
            renullMode=false;
          }else if(posSub=="1"){
            Serial.println("POS "+String(posX,DEC)+" "+String(posY,DEC)+" "+String(posZ,DEC));
          }else if(posSub=="2"){
            renullMode=true; 
            toX=1;
            toY=1;
          }else if(posSub=="3"){
            Serial.println(String(digitalRead(Y_MIN_PIN)));
          }else if(posSub=="4")
            pause=true;
          else if(posSub=="5")
            pause=false;
          else if(posSub=="6")
            Serial.println("POS "+String(posX/XSTEPMM,DEC)+" "+String(posY/YSTEPMM,DEC)+" "+String(posZ/ZSTEPMM,DEC));
          break;
  }
  switch(workMode){
    case 0:
    case 1:
      switch(buf[0]){
        case 'X':
          sendXTo(pos*XSTEPMM);
          break;
        case 'Y':
         sendYTo(pos*YSTEPMM);
         break;
        case 'Z':
          sendZTo(pos*ZSTEPMM);
          break;
        case 'F':
          f=Minute/(pos*XSTEPMM);
           Serial.println(f);
          break;
      }
      break;
   case 2:
   case 3:
     switch(buf[0]){
        case 'X':
          fX=pos;
          break;
        case 'Y':
         fY=pos;
         break;
        case 'I':
          I=pos;
        case 'J':
          J=pos;
          break;
        case 'F':
          f=Minute/(pos*XSTEPMM);
          Serial.println(f);
          break;
      }
      
     break;
  }
}

void serial(){
  if (Serial.available()) {
      char ch=0;
      String buf="";
      do{
        ch=Serial.read();
        if(ch==-1)
          continue;
        if(ch==' '){
          processCommand(buf);
          buf="";
        }else if(ch!='\n'){
          buf+=ch;
        }
      }while(ch!='\n');
      processCommand(buf);
      Serial.println("go.");
  }
}

void renullProc(){
  if(toX>0){
    if(digitalRead(X_MIN_PIN)==LOW){
      posX=0;
      toX=0;
      Serial.println("X HOME!");
    }
    dX=true;
    xstep=1;
  }
  if(toY>0){
    if(digitalRead(Y_MIN_PIN)==LOW){
      posY=0;
      toY=0;
      Serial.println("Y HOME!");
    }
    dY=true;
    ystep=1;
  }
  if(toX==0 && toY==0){
    renullMode=false;
  }
}


float halfStepsX = 0;
float halfStepsY=0;
float halfStepsZ=0;



void commandProcLinear(){
  int maxSteps=max(toX,max(toY,toZ));
  
     if(toX>0){
        xstep=true;
        posX+=dX?-1:1;
        toX-=1;
      }
      if(toY>0){
        ystep=true;
        posY+=dY?-1:1;
        toY-=1;
      }
      if(toZ>0){
        zstep=true;
        posZ+=dZ?-1:1;
        toZ-=1;
      }
}


boolean nidle=true;
long int idleSince=0;
void loop () {
    nidle=(toX==0 && toY==0 && toZ==0) && !circ;
    if(nidle && !idle){
      Serial.println("ok.");
      workMode=0;
      idleSince=millis();
    }
    idle=(toX==0 && toY==0 && toZ==0) && !circ;
    led();
    if(idle || (Serial.available() && Serial.peek()=='D'))
      serial();
    if(pause)
      return;
    if(renullMode){
      renullProc();
    }else{
      if((workMode==2 || workMode==3) && !circ){
        if(workMode==2){
          initG2();
        }else{
          initG3();
        }
      }
      if(circ)
        commandProcCircular();
      commandProcLinear();
    }
    boolean idleTime=idle &&(millis()-idleSince>5000);
    digitalWrite(X_ENABLE_PIN,idleTime);
    digitalWrite(Y_ENABLE_PIN,idleTime);
    digitalWrite(Z_ENABLE_PIN,idleTime);
    if(!idle){
      digitalWrite(X_DIR_PIN, dX?LOW:HIGH);
      digitalWrite(Y_DIR_PIN, dY?LOW:HIGH);
      digitalWrite(Z_DIR_PIN, dZ?LOW:HIGH);
      digitalWrite(X_STEP_PIN,!xstep);
      digitalWrite(Y_STEP_PIN,!ystep);
      digitalWrite(Z_STEP_PIN,!zstep);
      xstep=0;
      ystep=0;
      zstep=0;
      delayMicroseconds((long int)f);
      digitalWrite(X_STEP_PIN,HIGH);
      digitalWrite(Y_STEP_PIN,HIGH);
      digitalWrite(Z_STEP_PIN,HIGH);
    }
    
}
